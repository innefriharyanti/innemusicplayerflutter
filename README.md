# Inne Music Player

Inne Music Player app is made in Flutter and Android device is supported.

# Demo App

[APK](https://drive.google.com/drive/folders/1YXz88qv9owhwYH62YIUWABGWze6XUlvR?usp=sharing)

## Supported devices

Tested on Samsung A9 Android 10 (API 29).

## Supported Flutter Version

Flutter version: 1.7.8+hotfix.4

## Features

* [x] Android

  * [x] Retrieve songs
  * [x] Play / Pause songs
  * [x] Repeat / Shuffle
  * [x] Added Now Playing Screen for selected song
  * [x] Search
  * [x] Smooth UI
  * [x] Integration with iTunes Affliate API
  * [x] onComplete
  * [x] onDuration / onCurrentPosition


### Screenshots

<img src="flute1.png" height="300em" /> <img src="flute2.png" height="300em" /> <img src="flute3.png" height="300em" /> <img src="flute4.png" height="300em" />

* Note - This project use the iTunes affiliate API to develop a simple music player apps. Check the below link.

### iTunes affiliate API

[API](https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api)


## Getting Started

For help getting started with Flutter, view our online
[documentation](http://flutter.io/).

* [x] Installation Steps

  * [x] Download Flutter SDK (https://flutter.dev/docs/development/tools/sdk/releases?tab=macos)
  * [x] Create folder /[folder name]/[folder name]
  * [x] cd /[folder name]/[folder name]
  * [x] unzip ~/Downloads/flutter_macos_2.2.3-stable.zip
  * [x] export PATH="$PATH:`pwd`/flutter/bin"
  * [x] export PATH="$PATH:/[folder name]/[folder name]/flutter/bin"
  * [x] echo $PATH
  * [x] which flutter
  * [x] which flutter dart
  * [x] flutter doctor
  * [x] flutter doctor --android-licenses
  * [x] flutter devices
  * [x] flutter run

